
## Before run:

### Generate Marvel API key

It's necessary to add **Marvel API Key** to project. To generate a key (private), go to your [Marvel Account](https://developer.marvel.com/docs).

After generate, open `.env` and replace the `PUT_YOUR_MARVEL_PRIVATE_KEY` with the generated key.

## Install packages

To install the project packages, run command `yarn` or `npm install`

## Available Scripts
  
In the project directory, you can run:

### `yarn start` or `npm start`

Runs the app in the development mode. 

**(If you did't generated your key yet, go back to top of this Readme)**

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

### `yarn test` or `npm run test`

**The project isn't configured with tests, but you can run the command**

Launches the test runner in the interactive watch mode.\

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`or  `npm run build`

Builds the app for production to the `build` folder.\

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\

Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.  

### Netlify

The platform is configured with Netlify. So, every `master` push commit trigger a build.

The build is deployed in the url below:
[https://frontend-627835.netlify.app/](https://frontend-627835.netlify.app/)


## Features

- The platform is responsive;
- Integrated with Marvel API;
- Has 404's page 
- The platform uses the SVGs as icons;
- The pagination, search, filter and order are based by url params;
- The search has debounce event. If you pause type for 2 seconds, the search is submitted;
- The search is submitted by Enter key press too. In that way, will cancel previous debounce event;
- You can like until 5 heroes. If you achieve that count, you'll be unable to like anymore. For that, if still wanted to like a hero, will be needed to unlike another one;
- Your likes are saved in localStorage, so if you reload the page, the likes will persist
- You can navigate throught the pages using the pagination on end of page. 

## Know issues

- The platform doesn't prevent unknown pages. For example, if you enter the plataform with page bigger than the limit, the page will accept and won't do anything. Without redirect, without treatment for that;
- The platform isn't prepared to return a feedback to user if backend throw error;
- The API can return invalid date on comic request , so to avoid that, I show a placeholder instead of that.
