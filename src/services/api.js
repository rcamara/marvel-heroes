import { agent } from './agent';

export const api = {
	list: ({ offset, limit, orderBy, nameStartsWith }) =>
		agent.get('/characters', { offset, limit, orderBy, nameStartsWith }),

	details: ({ id }) =>
		agent.get(`/characters/${id}`),

	comics: ({ id, orderBy }) =>
		agent.get(`/characters/${id}/comics`, { orderBy }),
};
