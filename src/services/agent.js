import Axios from 'axios';
import config from '../config';

const axios = Axios.create({
	baseURL: config.apiUrl,
	params: {
		// every endpoint needs to request with Marvel API Key
		apikey: config.apiKey
	},
	headers: {
		'Content-type': 'application/json'
	}
});

const onResponse = (response) => response.data;

export const agent = {
	get: (endpoint, params) =>
		axios.get(endpoint, { params }).then(onResponse),

	post: (endpoint, body, params) =>
		axios.post(endpoint, body, { params }).then(onResponse),

	put: (endpoint, body, params) =>
		axios.put(endpoint, body, { params }).then(onResponse),

	delete: (endpoint, params) =>
		axios.delete(endpoint, { params }).then(onResponse),
};
