import {
  Switch,
  Route,
	Redirect,
	useLocation,
} from "react-router-dom";

import Template from './components/shared/Template';
import HomePage from './components/pages/Home';
import HeroPage from './components/pages/Hero';
import NotFoundPage from './components/pages/NotFound';

function App() {
	const location = useLocation();
	// Platform router
  return (
    <Route path="/">
      <Template>
        <Switch location={location}>
          <Route component={NotFoundPage} path="/not-found" exact />
          <Route component={HeroPage} path="/hero/:id" exact />
          <Route component={HomePage} path="/" exact />
          <Redirect from="*" to="/not-found" />
        </Switch>
      </Template>
    </Route>
  );
}

export default App;
