const storageLikeds = localStorage.getItem('::likes') && JSON.parse(localStorage.getItem('::likes'));

const initialState = {
		list: [],
		details: {},
		metadata: {},
		likeds: storageLikeds || {},
		loading: {},
		filters: {
			order: false,
			limit: 20,
			offset: 1,
			filter: false,
			query: undefined,
		}
};


export default initialState;
