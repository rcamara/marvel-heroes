import {
	fork,
	all,
	takeLatest,
	takeEvery,
	call,
	put,
	select
} from 'redux-saga/effects';

import format from 'date-fns/format';
import merge from 'lodash/merge';
import get from 'lodash/get';

import * as actions from './actions';
import * as selectors from './selectors';

import { api } from '../services/api';

export function* requestList () {
    yield takeLatest(actions.requestList, function* () {

			try {
				const { offset = 1, limit = 20, order, query } = yield select(selectors.getFilters);

			// trigger loading indicator
				yield put(actions.setLoading({ home: true }));

				// on fetch, get values retrieved from url params
				const { data: { results, ...metadata } } = yield call([api, api.list], {
					// every offset is calculated multiplying that value with current limit
					offset: (+offset - 1) * +limit,
					orderBy: order ? '-name' : undefined,
					// You can type "Hul" and the api will return the entries with name started with Hul. So will return "Hulk" entries
					nameStartsWith: query && query,
				});

				// on success, add list and metadata
				yield put(actions.setList(results));
				yield put(actions.setMetadata(metadata));

			} catch (e) {
			} finally {
				// hide loading indicator
				yield put(actions.setLoading({ home: false }));
			}

		});
}

export function* requestLike () {
	yield takeEvery(actions.requestLike, function* ({ payload }) {

		// get storaged likeds on store
		const likeds = yield select(state => {
			const likeds = selectors.getLikeds(state);
			return { ...likeds };
		});

		// check if has a entry with item id
		const liked = likeds[payload.id];

		// if is liked delete it from store. If don't create it.
		liked ?
			delete likeds[payload.id] :
			likeds[payload.id] = {
				id: payload.id,
				thumbnail: payload.thumbnail,
				name: payload.name,
			};

		// check count of likes. More than 5 will blocked
		if (liked || Object.keys(likeds).length <= 5) {
			yield put(actions.setLikeds({...likeds}));
			localStorage.setItem('::likes', JSON.stringify(likeds));
		}

	});
}

export function* requestFilter () {
	// every filter change, request again the heroes's list
	yield takeEvery(actions.setFilters, function* () {

		yield put(actions.requestList());

	});
}

export function* requestDetails () {
	yield takeEvery(actions.requestDetails, function* ({ payload }) {

		try {
			const { id } = payload;

			// trigger loading indicator
			yield put(actions.setLoading({ details: true }));

			// get hero's details
			const { data: details } = yield call([api, api.details], { id });

			// get hero's comics
			const { data: comics } = yield call([api, api.comics], { id, orderBy: '-onsaleDate' });

			// get first comic using get lodash
			const lastComic = get(comics, 'results[0].dates[0]', {});

			// try catch to prevent crash caused by invalid date retrieved by api
			try {
				lastComic.date = format(new Date(lastComic.date), 'dd MMM. yyyy');
			} catch (e) {
				// if throw error remove date
				lastComic.date = null;
			}

			// merge hero's details with comics data
			const hero = merge(details.results[0], {
				comics: {
					items: comics.results,
					last: lastComic,
				}
			});

			// set hero's details
			yield put(actions.setDetails(hero));

		} catch (e) {
		} finally {
			// hide loading indicator
			yield put(actions.setLoading({ details: false }));
		}

	});
}

export default function* () {
    yield all([
		fork(requestList),
		fork(requestLike),
		fork(requestFilter),
		fork(requestDetails),
    ]);
}
