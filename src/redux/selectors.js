import orderBy from 'lodash/orderBy';
import { createSelector } from 'reselect';

export const getState = (state) => state;

export const getResults = createSelector(getState, (state) => state.list) || [];

export const getMetadata = createSelector(getState, (state) => state.metadata) || {};

export const getLikeds = createSelector(getState, (state) => state.likeds) || {};

export const getFilters = createSelector(getState, (state) => state.filters) || {};

export const getList = createSelector(
	[getResults, getLikeds, getFilters],
	(results, likeds, filters) =>
		// if has filter, return ordered list
		filters.filter ?
			orderBy(Object.values(likeds), 'name', filters.order && 'desc') :
			results
);
export const getLiked = createSelector([getLikeds, (_, id) => id], (likeds, id) => !!likeds[id]);

export const getDetails = createSelector(getState, (state) => state.details) || {};

// check if liked action is enabled
export const getLikeStatus = createSelector(getLikeds,
	(likeds) => !likeds || Object.keys(likeds).length < 5
);

export const getLoading = createSelector(getState, (state) => state.loading) || {};
