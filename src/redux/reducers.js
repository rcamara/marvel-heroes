import updateState from 'immutability-helper';
import { handleActions } from 'redux-actions';
import initialState from './initial-state';
import * as actions from './actions';

const setList = (state, { payload }) => updateState(state, {
	list: {
		$set: payload
	}
});

const setMetadata = (state, { payload }) => updateState(state, {
	metadata: {
		$merge: payload
	}
});

const setLikeds = (state, { payload }) => updateState(state, {
	likeds: {
		$set: payload,
	}
});

const setFilters = (state, { payload }) => updateState(state, {
	filters: {
		$merge: payload,
	}
});

const setDetails = (state, { payload }) => updateState(state, {
	details: {
		$set: payload,
	}
});

const setLoading = (state, { payload }) => updateState(state, {
	loading: {
		$merge: payload
	}
});

const clearDetails = (state) => updateState(state, {
	details: {
		$set: {},
	}
});

const reducers = {
    [actions.setList]: setList,
    [actions.setMetadata]: setMetadata,
		[actions.setLikeds]: setLikeds,
		[actions.setFilters]: setFilters,
		[actions.setDetails]: setDetails,
		[actions.setLoading]: setLoading,
		[actions.clearDetails]: clearDetails,
};

export default handleActions(
    reducers,
    initialState,
);
