import { createAction } from 'redux-actions';

export const requestList = createAction('REQUEST/LIST');
export const requestLike = createAction('REQUEST/LIKED');
export const requestDetails = createAction('REQUEST/DETAILS');

export const setList = createAction('SETTER/LIST');
export const setMetadata = createAction('SETTER/METADATA');
export const setLikeds = createAction('SETTER/LIKEDS');
export const setFilters = createAction('SETTER/FILTERS');
export const setDetails = createAction('SETTER/DETAILS');
export const setLoading = createAction('SETTER/LOADING');

export const clearDetails = createAction('CLEAR/DETAILS');
