// get variables from .env
const config = {
    apiKey: process.env.REACT_APP_API_KEY,
    apiUrl: `${process.env.REACT_APP_API_URL}v1/public/`,
};

export default config;
