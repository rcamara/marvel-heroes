import { useSelector } from 'react-redux';
import { getLoading } from '../../redux/selectors';

function LoadingIndicator({ id }) {

	const loading = useSelector(getLoading);

	if (!loading[id])
		return null;

	return <span className='loading-indicator' />;
}

export default LoadingIndicator;
