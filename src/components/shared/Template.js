import React from 'react';

function Template({ children }) {

    return (
        <>
            <main id="main">{children}</main>
            <footer id="footer"></footer>
        </>
    );
}

export default Template;
