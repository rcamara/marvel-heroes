import React from 'react';
import { NavLink } from "react-router-dom";

function Navigation() {
  return (
    <nav className="container">
      <NavLink to="/">Home</NavLink>
      <NavLink to="/hero/2">Hero</NavLink>
      <NavLink to="/not-found">Not Found</NavLink>
    </nav>
  );
}

export default Navigation;
