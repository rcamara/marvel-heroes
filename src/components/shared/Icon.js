import { ReactComponent as IconBook } from '../../assets/svg/icon-book.svg';
import { ReactComponent as IconHero } from '../../assets/svg/icon-hero.svg';
import { ReactComponent as IconLikeOff } from '../../assets/svg/icon-like-off.svg';
import { ReactComponent as IconLikeOn } from '../../assets/svg/icon-like-on.svg';
import { ReactComponent as IconSearch } from '../../assets/svg/icon-search.svg';
import { ReactComponent as IconStar } from '../../assets/svg/icon-star.svg';
import { ReactComponent as IconVideo } from '../../assets/svg/icon-video.svg';

const ICONS = {
	book: IconBook,
	hero: IconHero,
	'like-off': IconLikeOff,
	'like-on': IconLikeOn,
	search: IconSearch,
	star: IconStar,
	video: IconVideo,
	default: () => null,
};

function Icon ({ name, className }) {

	const Component = ICONS[name] || ICONS.default;

	return <Component className={['icon', className].join(' ')}/>;
}

export default Icon;
