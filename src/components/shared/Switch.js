function Switch({ onChange, checked }) {

  return (
    <button
      onClick={onChange}
      className={["switch-input", checked && 'checked'].join(' ')} />
  );
}

export default Switch;
