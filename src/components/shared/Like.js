import Icon from './Icon';

function LikeAction ({ onClick, disabled, liked, className, title, alt }) {

  return (
		<button
			onClick={onClick}
			className={['like-action', liked && 'liked', className].join(' ')}
			disabled={disabled}
			title={title}
			alt={alt}>
			<Icon name={`like-${liked ? 'on' : 'off'}`} />
		</button>
  );
}

export default LikeAction;
