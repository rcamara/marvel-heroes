import { NavLink } from 'react-router-dom';
import Logo from './Logo';

function Header({ children, inline }) {

  return (
		<header id="header" className={inline && 'inline'}>
			<div className={["container", inline && 'full'].join(' ')}>
				<NavLink to="/">
					<Logo inline={inline} />
				</NavLink>
				{children}
			</div>
		</header>
  );
}

export default Header;
