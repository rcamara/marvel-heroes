import { ReactComponent as LogoDefault } from '../../assets/svg/logo.svg';
import { ReactComponent as LogoInline } from '../../assets/svg/logo.inline.svg';

const LOGOS = {
	inline: LogoInline,
	default: LogoDefault,
};

function Logo ({ inline }) {

	const Component = inline ? LOGOS.inline : LOGOS.default;

	return <Component className="logo" />;
}

export default Logo;
