import React from 'react';
import debounce from 'lodash/debounce';
import { connect } from 'react-redux';
import {  withRouter } from 'react-router';
import Icon from './Icon';

function SearchInput ({ dispatchEvent, debounceEvent, defaultValue }) {

	const [value, setValue] = React.useState(defaultValue);
	const ref = React.useRef();

	const onSubmit = (ev) => {
		ev.preventDefault();
		dispatchEvent(value);
	};

	const onChange = (ev) => {
		const { value } = ev.target;

		value ?
			debounceEvent(value):
			dispatchEvent(value);

		debounceEvent(value);
		setValue(value);
	};

	React.useEffect(() => {
		setValue(defaultValue);
	}, [defaultValue]);

  return (
		<form
			className="form-input"
			onSubmit={onSubmit}
			ref={ref}>
			<label htmlFor="search-input">
				<Icon name="search" />
			</label>
			<input
				id="search-input"
				type="search"
				placeholder="Procure por heróis"
				value={value}
				onChange={onChange} />
		</form>
  );
}

const mapToProps = (_, { history, location }) => {
	const params = new URLSearchParams(location.search);

	const dispatchEvent = (value) => {

		// cancel last debounce event
		debounceEvent.cancel();

		// if has value set it, if don't remove it
		value ?
			params.set('search', value):
			params.delete('search');

		// reset page and filter param
		params.delete('page');
		params.delete('filter');

		history.push(`/?${params.toString()}`);
	};

	const debounceEvent = debounce(dispatchEvent, 1500);

	return {
		defaultValue: params.get('search') || '',
		dispatchEvent,
		debounceEvent,
	};
};

export default withRouter(
	connect(null, mapToProps)(SearchInput)
);
