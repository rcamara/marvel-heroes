import React from 'react';
import { NavLink } from "react-router-dom";
import { useLocation } from 'react-router';
import { useSelector } from 'react-redux';

import { getMetadata, getFilters } from '../../redux/selectors';

function Pagination() {

	const { filter } = useSelector(getFilters);
	const {
		limit = 20,
		offset = 0,
		total = 0,
		count = 0 } = useSelector(getMetadata);
	const location = useLocation();

	if (filter)
		return null;

	const prevParams = new URLSearchParams(location.search);
	const nextParams = new URLSearchParams(location.search);
	const firstParams = new URLSearchParams(location.search);
	const lastParams = new URLSearchParams(location.search);

	const totalPages = Math.ceil(total / limit) || 1;
	const currPage = Math.ceil((offset + count) / limit) || 1;
	const prevPage = currPage - 1;
	const nextPage = currPage + 1;

	prevParams.set('page', prevPage);
	nextParams.set('page', nextPage);
	firstParams.set('page', 1);
	lastParams.set('page', totalPages);

	const PrevLink = prevPage ?
		NavLink :
		(props) => <span {...props} disabled />;

	const NextLink = nextPage <= totalPages ?
		NavLink :
		(props) => <span {...props} disabled />;

	if (totalPages <= 1)
		return null;

  return (
    <div className="pagination">
			<PrevLink
				className="pagination-link"
				to={`/?${firstParams.toString()}`}
				title="Voltar para a primeira página">{'<<'}</PrevLink>

			<PrevLink
				className="pagination-link"
				to={`/?${prevParams.toString()}`}
				title="Voltar uma página">Anterior</PrevLink>
				<span>{currPage} / {totalPages}</span>

			<NextLink
				className="pagination-link"
				to={`/?${nextParams.toString()}`}
				title="Avançar uma página">Próxima</NextLink>

			<NextLink
				className="pagination-link"
				to={`/?${lastParams.toString()}`}
				title="Avançar para a última página">{'>>'}</NextLink>

    </div>
  );
}

export default Pagination;
