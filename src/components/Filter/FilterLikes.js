import Like from '../shared/Like';

function FilterLikes({ onChange, enabled }) {

	const onChangeEvent = (ev) => {
		ev.preventDefault();
		onChange('filter', 'likes');
	};

	return (
		<div className="filter-likes">
			<Like onClick={onChangeEvent} liked={enabled} />
			<span>Somente favoritos</span>
		</div>
	);
}

export default FilterLikes;
