import { useLocation, useHistory } from 'react-router';
import { useSelector } from 'react-redux';

import FilterOrder from './FilterOrder';
import FilterLikes from './FilterLikes';
import { getMetadata, getFilters, getList } from '../../redux/selectors';

function Filter() {
	const likes = useSelector(getList);
	const filters = useSelector(getFilters);
	const metadata = useSelector(getMetadata);
	const location = useLocation();
	const history = useHistory();
	const params = new URLSearchParams(location.search);

	const onChange = (key, value) => {
		params.has(key) ?
			params.delete(key) :
			params.set(key, value);

		history.replace(`${location.pathname}?${params.toString()}`);
	};

	return (
		<div className="filter-container">
			<span>Exibindo {(filters.filter ? likes.length : metadata.count) || 0} heróis</span>
			<FilterOrder onChange={onChange} enabled={params.has('order')} />
			<FilterLikes onChange={onChange} enabled={params.has('filter')} />
		</div>
	);
}

export default Filter;
