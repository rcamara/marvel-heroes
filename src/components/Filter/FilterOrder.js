import Icon from '../shared/Icon';
import Switch from '../shared/Switch';

function FilterOrder ({ onChange, enabled }) {

	const onChangeEvent = (ev) => {
		ev.preventDefault();
		onChange('order', '-name');
	};

  return (
		<div className="filter-order">
			<Icon name="hero" />
			<span>Ordenar por nome - A/Z</span>
			<Switch onChange={onChangeEvent} checked={enabled} />
		</div>
  );
}

export default FilterOrder;
