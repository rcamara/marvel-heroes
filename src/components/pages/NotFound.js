import React from 'react';
import { NavLink } from 'react-router-dom';
import Header from '../shared/Header';

function NotFoundPage() {

	return (
		<>
			<Header />
			<div id="not-found-page" className="container">
				<p className="text-center">
					Unfortunately the page requested was not found
				</p>
				<NavLink to="/" className="btn">
					Go to home
				</NavLink>
			</div>
		</>
	);
}

export default NotFoundPage;
