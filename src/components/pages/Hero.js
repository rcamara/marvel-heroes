import React from 'react';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';

import Header from '../shared/Header';
import SearchInput from '../shared/SearchInput';
import LoadingIndicator from '../shared/LoadingIndicator';
import HeroDetails from '../HeroDetails';
import HeroComics from '../HeroComics';

import { requestDetails, clearDetails } from '../../redux/actions';

function HeroPage({ dispatch }) {

		const { id } = useParams();

		// On mount add class to #root
		React.useEffect(() => {
			document.querySelector('#root').classList.add('details-page');
			return () => {
				dispatch(clearDetails());
				// On unmount remove class from #root
				document.querySelector('#root').classList.remove('details-page');
			};
		}, []);

		React.useEffect(() => {
			// on mount request hero's data
			dispatch(requestDetails({ id }));
		}, [id]);

    return (
			<>
				<Header inline>
					<SearchInput />
				</Header>
				<LoadingIndicator id="details" />
				<div className="container full">
					<HeroDetails />
				</div>
				<div className="container full">
					<HeroComics />
				</div>
			</>
    );
}

export default connect()(HeroPage);
