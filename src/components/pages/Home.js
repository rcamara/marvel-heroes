import React from 'react';

import { connect } from 'react-redux';
import { useLocation } from 'react-router';

import Header from '../shared/Header';
import SearchInput from '../shared/SearchInput';
import Pagination from '../shared/Pagination';
import LoadingIndicator from '../shared/LoadingIndicator';
import Filter from '../Filter';
import HeroesList from '../HeroesList';

import { setFilters } from '../../redux/actions';

function HomePage({ dispatch }) {

	const location = useLocation();

	// Update filter according url params
	React.useEffect(() => {
		const params = new URLSearchParams(location.search);
		// on filter update, the platform automatically will trigger the heroes's request
    dispatch(setFilters({
			order: params.has('order'),
			offset: params.get('page') || 1,
			filter: params.has('filter'),
			query: params.get('search')
		}));
	}, [location.search]);

  return (
		<>
			<Header />
			<div className="container">
				<h3 className="text-uppercase text-center">Explore o mundo</h3>
				<p className="text-center">Mergulhe no domínio deslumbrante de todos os personagens clássicos que você ama - e aqueles que você descobrirá em breve!</p>
				<SearchInput />
			</div>
			<div className="container full">
				<Filter />
				<LoadingIndicator id="home" />
				<HeroesList />
			</div>
			<div className="container full">
				<Pagination />
			</div>
		</>
  );
}

export default connect()(HomePage);
