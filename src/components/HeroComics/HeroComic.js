function HeroComic ({ item }) {

	const { thumbnail, name } = item;

	return (
		<li className="hero-comic">
			<div>
				<figure>
						<img
							src={`${thumbnail.path}/portrait_xlarge.${thumbnail.extension}`}
							title={`Comic ${name}`}
							alt={`Comic ${name}`} />
				</figure>
				<strong>{name}</strong>
			</div>
		</li>
	);
}

export default HeroComic;
