import { useSelector } from 'react-redux';
import { getDetails } from '../../redux/selectors';

import HeroComic from './HeroComic';

function HeroPublishments () {

	const { comics = {} } = useSelector(getDetails);

	if (!comics.items?.length)
		return null;

	const renderComic = (comic) =>
		<HeroComic key={`hero-comic-${comic.id}`} item={comic} />;

	return (
		<>
			<h3>Últimos lançamentos</h3>
			<ul className="hero-comics">
				{comics.items?.slice(0, 10).map(renderComic)}
			</ul>
		</>
	);
}

export default HeroPublishments;
