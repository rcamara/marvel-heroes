import { useSelector, connect } from 'react-redux';
import { getDetails, getLiked, getLikeStatus } from '../../redux/selectors';
import { requestLike } from '../../redux/actions';

import Like from '../shared/Like';
import HeroMetaInfo from './HeroMetaInfo';

function HeroDetails({ dispatch }) {

	const { description, name, thumbnail = {}, id } = useSelector(getDetails);
	const liked = useSelector((state) => getLiked(state, id));
	const likeIsEnaled = useSelector(getLikeStatus);

	// generate like description if enabled or not
	const likeTitle = !liked && !likeIsEnaled ?
		`Você já favoritou 5 heróis. Para poder favoritar ${name}, é necessário desfavoritar um.`:
		`${!liked ? 'Favoritar' : 'Desfavoritar'} ${name}.`;

	const onLike = (ev) => {
		ev.preventDefault();
		// on like send hero data
		dispatch(requestLike({ name, id, thumbnail }));
	};

	if (!name)
		return null;

	return (
		<div className="hero-description" data-name={name}>
			<div className="hero-info">
				<div className="hero-info-header">
					<h4 className="hero-info-name">{name}</h4>
					<Like
						onClick={onLike}
						liked={liked}
						disabled={!liked && !likeIsEnaled}
						title={likeTitle}
						alt={likeTitle} />
				</div>
				<p>{description}</p>
				<HeroMetaInfo />
			</div>
			<div className="hero-image">
				{thumbnail.path ? (
					<img
						src={`${thumbnail.path}/standard_fantastic.${thumbnail.extension}`}
						title={`${name}'s picture`}
						alt={`${name}'s picture`} />
				) : null}
			</div>
		</div>
	);
}

export default connect()(HeroDetails);
