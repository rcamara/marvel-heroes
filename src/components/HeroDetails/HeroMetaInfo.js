import { useSelector } from 'react-redux';

import { getDetails } from '../../redux/selectors';

import Icon from '../shared/Icon';

function HeroMetaInfo() {

	const { comics = {}, series = {} } = useSelector(getDetails);

	const Stars = new Array(5)
		.fill(Icon)
		.map((Component, idx) =>
			<Component key={`star-${idx}`} name="star" className={idx < 3 && 'active'} />
		);

	return (
		<table className="hero-metainfo">
			<tbody>
				<tr>
					<td className="hero-metainfo-books">
						<strong>Quadrinhos</strong>
						<div>
							<Icon name="book" />
							{comics.available || 0}
						</div>
					</td>
					<td className="hero-metainfo-movies">
						<strong>Filmes</strong>
						<div>
							<Icon name="video" />
							{series.available || 0}
						</div>
					</td>
				</tr>
				<tr>
					<td colSpan={2} className="hero-metainfo-rating">
						<strong>Rating:</strong>
						{Stars}
					</td>
				</tr>
				{comics.last?.date ? (
					<tr>
						<td colSpan={2} className="hero-metainfo-last-published">
							<strong>Último quadrinho:</strong>
							<span className="text-lowercase">{comics.last?.date}</span>
						</td>
					</tr>
				) : null}
			</tbody>
		</table>
	);
}

export default HeroMetaInfo;
