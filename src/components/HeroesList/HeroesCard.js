import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { requestLike } from '../../redux/actions';
import { getLiked, getLikeStatus } from '../../redux/selectors';

import Like from '../shared/Like';

function HeroesCard({ dispatch, item, liked, likeIsEnaled }) {

	const { thumbnail, name, id } = item;

	// generate like description if enabled or not
	const likeTitle = !liked && !likeIsEnaled ?
		`Você já favoritou 5 heróis. Para poder favoritar ${name}, é necessário desfavoritar um.`:
		`${!liked ? 'Favoritar' : 'Desfavoritar'} ${name}.`;

	const onClick = (ev) => {
		ev.preventDefault();
		ev.stopPropagation();
		dispatch(requestLike(item));
	};

	return (
		<li className="heroes-list-item">
			<Link to={`/hero/${id}`}>
				<figure>
					<img src={`${thumbnail.path}/standard_fantastic.${thumbnail.extension}`} />
				</figure>
				<div className="heroes-item-info">
					<strong className="heroes-item-name">{name}</strong>
					<Like
						onClick={onClick}
						liked={liked}
						disabled={!liked && !likeIsEnaled}
						title={likeTitle}
						alt={likeTitle}  />
				</div>
			</Link>
		</li>
	);
}

const mapToState = (state, props) => ({
	liked: getLiked(state, props.item?.id),
	likeIsEnaled: getLikeStatus(state),
});

export default connect(mapToState)(HeroesCard);
