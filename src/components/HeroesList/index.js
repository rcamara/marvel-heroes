import React from 'react';
import { connect } from 'react-redux';
import { getList, getFilters, getLoading } from '../../redux/selectors';

import HeroesCard from './HeroesCard';

function HeroesList({ list, filters, loading }) {

	const renderCard = (item) => (
		<HeroesCard item={item} key={`heroes-list-item-${item.id}`} />
	);

	let notFoundMessage = null;

	// hide content if is loading
	if (loading.home) {
		return null;
	}

	// if hasn't list add message feedback to user
	if (!list.length) {

		// if has some filter setted, add the message below
		if (filters.filter) {
			notFoundMessage = 'Você ainda não favoritou um herói. Volte para a nossa lista, e favorite o seu primeiro.';
		}

		// if a search is setted, add the message below
		if (filters.query) {
			notFoundMessage = `Não encontramos um herói com nome "${filters.query}". Refina sua busca, e tente novamente`;
		}

		// if has message, return message
		if (notFoundMessage) {
			return <strong className="heroes-not-found text-center text-red">{notFoundMessage}</strong>;
		}
	}

	return <ul className="heroes-list">{list.map(renderCard)}</ul>;
}

const mapsToState = (state) => ({
	list: getList(state),
	filters: getFilters(state),
	loading: getLoading(state),
});

export default connect(mapsToState)(HeroesList);
